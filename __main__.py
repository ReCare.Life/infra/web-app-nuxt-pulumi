import json

import boto3
import pulumi
import pulumi_cloudflare as cloudflare
from pulumi_aws import acm, apigatewayv2, iam, lambda_

resource_name_prefix = "recare"
config = pulumi.Config()
org = config.require("org")
domain_names = config.require_object("domainNames")
stack_name = pulumi.get_stack()
image_repo_stack = pulumi.StackReference(f"{org}/image-repo/prod")
cloudflare_zone_id = config.require_secret("cloudflareZoneId")
ecr_client = boto3.client("ecr")

if stack_name == "dev":
    env = "dev"
else:
    env = "prod"

web_app_nuxt_lambda_iam_role = iam.Role(
    f"{resource_name_prefix}-web-app-nuxt",
    assume_role_policy=json.dumps(
        {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Sid": "lambda",
                    "Action": "sts:AssumeRole",
                    "Principal": {"Service": "lambda.amazonaws.com"},
                    "Effect": "Allow",
                }
            ],
        }
    ),
    tags={"Environment": env}
)


def get_ecr_repo_image_disgest(ecr_repo_name):
    response = ecr_client.describe_images(
        repositoryName=ecr_repo_name,
        imageIds=[
            {
                "imageTag": env
            }
        ]
    )
    digest = response["imageDetails"][0]["imageDigest"]
    return digest


web_app_nuxt_lambda = lambda_.Function(
    f"{resource_name_prefix}-web-app-nuxt",
    role=web_app_nuxt_lambda_iam_role.arn,
    package_type="Image",
    image_uri=image_repo_stack.get_output("webAppNuxtLambdaEcrRepoUrl").apply(
        lambda ecr_repo_url: f"""{ecr_repo_url}@{get_ecr_repo_image_disgest(ecr_repo_url.split("/")[-1])}"""
    ),
    memory_size=128,
    timeout=10,
    tags={"Environment": env},
)

web_app_nuxt_api_gw = apigatewayv2.Api(
    f"{resource_name_prefix}-web-app-nuxt",
    protocol_type="HTTP",
    version="1.0.0",
    tags={"Environment": env}
)

web_app_nuxt_api_gw_lambda_integration = apigatewayv2.Integration(
    f"{resource_name_prefix}-web-app-nuxt",
    api_id=web_app_nuxt_api_gw.id,
    integration_type="AWS_PROXY",
    integration_uri=web_app_nuxt_lambda.invoke_arn,
    connection_type="INTERNET",
    integration_method="POST"
)

web_app_nuxt_lambda_api_gw_permission = lambda_.Permission(
    f"{resource_name_prefix}-web-app-nuxt",
    action="lambda:InvokeFunction",
    function=web_app_nuxt_lambda.name,
    principal="apigateway.amazonaws.com",
    source_arn=web_app_nuxt_api_gw.execution_arn.apply(
        lambda x: f"{x}/*/*"
    )
)

web_app_nuxt_api_gw_route = apigatewayv2.Route(
    f"{resource_name_prefix}-web-app-nuxt",
    api_id=web_app_nuxt_api_gw.id,
    route_key="$default",
    target=web_app_nuxt_api_gw_lambda_integration.id.apply(
        lambda id: f"integrations/{id}"
    )
)

web_app_nuxt_api_gw_stage = apigatewayv2.Stage(
    f"{resource_name_prefix}-web-app-nuxt",
    name="$default",
    api_id=web_app_nuxt_api_gw.id,
    auto_deploy=True
)

web_app_nuxt_api_acm_cert = acm.Certificate(
    f"{resource_name_prefix}-web-app-nuxt",
    domain_name=domain_names[0],
    subject_alternative_names=domain_names[1:],
    validation_method="DNS",
    tags={
        "Environment": env,
    }
)

web_app_nuxt_cloudflare_dns_records = web_app_nuxt_api_acm_cert.domain_validation_options.apply(
    lambda domain_validation_options: [
        cloudflare.Record(
            f"{resource_name_prefix}-web-app-nuxt-cert-{index}",
            zone_id=cloudflare_zone_id,
            name=domain_validation_option.resource_record_name,
            value=domain_validation_option.resource_record_value,
            type=domain_validation_option.resource_record_type,
            ttl=3600
        )
        for index, domain_validation_option in enumerate(domain_validation_options)
    ]
)

web_app_nuxt_api_gw_domain_name = apigatewayv2.DomainName(
    f"{resource_name_prefix}-web-app-nuxt",
    domain_name=domain_names[0],
    domain_name_configuration=apigatewayv2.DomainNameDomainNameConfigurationArgs(
        certificate_arn=web_app_nuxt_api_acm_cert.arn,
        endpoint_type="REGIONAL",
        security_policy="TLS_1_2"
    ),
    tags={
        "Environment": env,
    }
)

cloudflare.Record(
    f"{resource_name_prefix}-web-app-nuxt-domain",
    zone_id=cloudflare_zone_id,
    name=domain_names[0],
    value=web_app_nuxt_api_gw_domain_name.domain_name_configuration.target_domain_name,
    type="CNAME",
    proxied=True,
    ttl=1
)

for index, domain_name in enumerate(domain_names[1:]):
    cloudflare.Record(
        f"{resource_name_prefix}-web-app-nuxt-domain-{index}",
        zone_id=cloudflare_zone_id,
        name=domain_name,
        value=domain_names[0],
        type="CNAME",
        proxied=True,
        ttl=1
    )

web_app_nuxt_api_gw_api_mapping = apigatewayv2.ApiMapping(
    f"{resource_name_prefix}-web-app-nuxt",
    api_id=web_app_nuxt_api_gw.id,
    domain_name=web_app_nuxt_api_gw_domain_name.id,
    stage=web_app_nuxt_api_gw_stage.id
)

pulumi.export("webAppNuxtApiGwUrl", web_app_nuxt_api_gw.api_endpoint)
